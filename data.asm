VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ; Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8230	;field A    
	dc.w $8300	;$833e	
	dc.w $8406	;field B	
	dc.w $8578	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8A00
	dc.w $8B00		
	dc.w $8C81	;81 normal 89 shadow and highlight
	dc.w $8D3F  ;h scroll		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
palette:
	dc.w $0000,$0000,$0000,$00A0,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000	
background_palette:
	dc.w $0000,$0000,$00A0,$0AAA,$0222,$0EEE,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
player_palette:
	dc.w $0EEE,$00E0,$0080,$0000,$000E,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
enemies_palette:
	dc.w $0EEE,$0AAA,$0000,$000E,$00E0,$0E00,$00A0,$00EE
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
	
palette_splash:
	dc.w $0000,$0000,$0662,$0222,$0200,$0686,$0666,$0EC8
	dc.w $0886,$0888,$0C86,$0C82,$0EEE,$06CC,$0CC6,$08CE ;mode 5
	dc.w $0000,$0000,$0000,$0eee,$026A,$000E,$0AAA,$02A2 ;text
	dc.w $006E,$0CAE,$048C,$0000,$086C,$00EE,$0000,$0000
	dc.w $0000,$0000,$0000,$0eee,$026A,$000E,$0AAA,$02A2 ;text
	dc.w $006E,$0CAE,$048C,$0000,$086C,$00EE,$0000,$0000	
	
splashtext:
 dc.b "Visit www.mode5.net$"
	
titletext:	
 dc.b "                                        "
 dc.b "       Error Rush by Mode5 games        "
 dc.b " How to play:                           "
 dc.b " Use the d-pad to move your character   "
 dc.b " and dodge the enemies. Hold 'A' to     "
 dc.b " move faster. The CPU in the middle can "
 dc.b " act as a safe zone, but it will pause  "  
 dc.b " the clock. The object of the game is   "
 dc.b " to last as long as possible.           " 
 dc.b "                 LLLLLL                 "
 dc.b "                |      |                "
 dc.b "                |-O--O-|                "
 dc.b "                |      |                "
 dc.b "                |\/\/\/|                "
 dc.b "                |/\/\/\|                "
 dc.b "                                        "
 dc.b "     Programmer's secret settings:      "
 dc.b "      Press 'A' to enable/disable       " 
 dc.b "                                        "
 dc.b "               Easy mode - OFF          " 
 dc.b "           Invincibility - OFF          "
 dc.b "        CPU load counter - OFF          "
 dc.b " Enemy spawner (press C) - OFF          " 
 dc.b " Enable safe middle zone - OFF          " 
 dc.b "                                        " 
 dc.b "          (C)2015 mode5 - www.mode5.net " 
 dc.b "            <-- Page 1/3 -->            " 
 dc.b "                                       %"
 
enemytext:
 dc.b "                                        "
 dc.b "            Know your enemy!            " 
 dc.b "       Address error -                  "
 dc.b "                                        "
 dc.b "        Zero Divide  -                  "
 dc.b "                                        "
 dc.b " Privilege violation -                  "
 dc.b "                                        " 
 dc.b " Illegal instruction -                  "
 dc.b "                                        " 
 dc.b " Address error is your standard enemy.  "
 dc.b " It moves toward you at a slow and      "
 dc.b " steady pace.                           "
 dc.b "                                        "
 dc.b " Zero divide fast and unpredictable.    "
 dc.b " It's nature is not fully understood,   "
 dc.b " but mathematicians claim zero divide   "
 dc.b " to be undefined.                       "
 dc.b "                                        "
 dc.b " Privilege violation has a similar      "
 dc.b " movement pattern to address error, but "
 dc.b " moves faster vertically.               "
 dc.b "                                        " 
 dc.b " Illegal instruction is a nasty enemy.  "
 dc.b " It moves at double speed, then slows   "
 dc.b " down and becomes partly invisible.     "
 dc.b "            <-- Page 2/3 -->            "
 dc.b "                                       %"

abouttext: 
 dc.b "                                        " 
 dc.b "            About this game:            " 
 dc.b "                                        " 
 dc.b " This game began as a test program for  " 
 dc.b " sprite collision detection. It was     " 
 dc.b " later expanded to make one sprite      " 
 dc.b " constantly follow another. I then      " 
 dc.b " created a system to create and animate " 
 dc.b " an arbitrary number of sprites. It was " 
 dc.b " then that I decided to try and create  " 
 dc.b " a game out of a test program.          " 
 dc.b "                                        " 
 dc.b "                                        " 
 dc.b "                                        " 
 dc.b "                                        " 
 dc.b "                                        " 
 dc.b "                                        " 
 dc.b "                                        " 
 dc.b "                                        " 
 dc.b " Build date: November 19, 2015 11PM     " 
 dc.b " Unpadded ROM size: 103,175 bytes       " 
 dc.b " Programming language: 68K assembly     " 
 dc.b " Number of instructions used: ~1500     "  
 dc.b " Programming, graphics, music, design,  " 
 dc.b " and testing by: James Lopez            "  
 dc.b "                                        " 
 dc.b "            <-- Page 3/3 -->            "  
 dc.b "                                       %"  
 
crash_screen:	
	dc.b "               GAME OVER!                                       "	
	dc.b " You ran straight into an enemy and                             "
	dc.b " caused your poor Genesis's CPU to                              "
	dc.b " kill itself. Here is what the CPU was                          "
	dc.b " thinking of at the moment of death.                            "
	dc.b "                                                                "			
	dc.b "----------------------------------------------------------------"
	dc.b "  Killed by :                                                   "
    dc.b "                                                                "
	dc.b "           *Data registers*                                     "	
	dc.b "                                                                "
	dc.b " D0:$???????? D1:$???????? D2:$????????                         "
	dc.b " D3:$???????? D4:$???????? D5:$????????                         "
	dc.b " D6:$???????? D7:$????????                                      "
	dc.b "                                                                "
	dc.b "          *Address registers*                                   "	
	dc.b "                                                                "
	dc.b " A0:$???????? A1:$???????? A2:$????????                         "
	dc.b " A3:$???????? A4:$???????? A5:$????????                         "
	dc.b " A6:$???????? A7:$????????                                      "
	dc.b "                                                                "
	dc.b "          *Special registers*                                   "
	dc.b "                                                                "
	dc.b "        Program counter:$??????                                 "
	dc.b "        Status register:$????                                   "
	dc.b "                                                                "
	dc.b "       Press START to play again!                               "
	dc.b "                                                                " 

music2: 
 incbin "sound\groovy.vgm"
 
sfx_slogan:
 incbin "sound\slogan.pcm"
 
hudtext:
 dc.b "Time-XX:XX Enemies-XX$"
	
sprites:
 incbin "gfx\playersprite.bin"
 incbin "gfx\enemies.bin"

font:
 incbin "gfx\ascii.bin"
hexfont: 
 incbin "gfx\hexascii.bin"
 
background:	
 incbin "gfx\background.kos"
 
splashscreen:
 incbin "gfx\splash.kos" 