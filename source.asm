    include "ramdat.asm"
    include "header.asm"

start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.l #$FFFFFE,a7
		move.w #$2700, sr       ;disable ints
 		move.w #$100,($A11100)
		move.w #$100,($A11200) 		
		bsr clear_regs
		bsr clear_ram
        bsr setup_vdp
		bsr region_check
		bsr clear_vram
		
		move.w #$0000, enemies ;for testing
		move.w #$0003, speed
		move.w #$4500,playerframe
		move.b #$01, enemyframe
		
		lea (splashscreen),a5
		lea (decompression_buffer), a1
        bsr decompress			
        move.l  #$70000000,(a3)  ;set VRAM write    
        move.w  #$4600, d4
		lea (decompression_buffer),a5
		bsr vram_loop		
		move.l #$40000003,(a3)	;vram write $c000	
		bsr generate_map_splash
		
		lea (font),a5
		move.l #$40000000,(a3)
		move.w #$07FF,d4
		bsr vram_loop	
		
		lea (palette_splash),a5
		move.w #$002f,d4
		move.l #$c0000000,(a3)
		bsr vram_loop
		move.w #$8230,(a3)
		move.w #$FFBE, d7		
		move.b #$ff,splashflag		
        move.w #$2300, sr       ;enable ints		
		
splashwait:
		nop
		tst splashflag
		bne splashwait
		move.w #$2700, sr	
		lea (splashtext),a5
		move.l #$0000cd16,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr overlay_text
		move.w #$0001,d4	
	    move.b #$2b,$A04000         ;DAC enable register             
	    move.b #$80,$A04001         ;enable DAC	
	    move.b #$2a,$A04000	
		lea (sfx_slogan),a5
		move.w #$FFFF,d4		 ;comment out to skip pause
splashhold:
		bsr test2612
		move.b (a5)+,$A04001
		move.w #$000C,d0
		bsr delay22khz
		dbf d4,splashhold	

        move.l  #$58000003,(a3)  ;set VRAM write    		
		lea (hexfont),a5
		move.w #$0400,d4
		bsr vram_loop	
		
		move.l #$60000002,(a3)
		lea (sprites),a5
		move.w #$0640,d4
		bsr vram_loop
		lea (palette),a5
		move.l #$c0000000,(a3)
		move.w #$003f,d4
		bsr vram_loop
		
		bsr titlescreen	

		lea (background),a5
		lea (decompression_buffer),a1
		bsr decompress
        move.l  #$50000000,(a3)  ;set VRAM write    
        move.w  #$4600, d4
		lea (decompression_buffer),a5
		bsr vram_loop		

		bsr create_map		
		bsr setup_sprites
			
		move.l #$40960003,(a3) ;C096
		lea (hudtext),a5
		bsr termtextloop	
		; lea (music_loop)+66,a1
		; move.l a1,vgm_start	
		; lea (music_intro)+66,a1
		lea (music2)+66,a1
		move.l a1,vgm_start				
        move.w #$2300, sr       ;enable ints		
		
loop:
		move.b #$ff,vb_flag	
		move.w #$0000,freetime	
		bsr check_box		
		bsr track
		bsr collision
		bsr animate		
		bsr clock
		bsr addenemies
		bsr randomnum
		bsr music_driver		
vb_wait:
		add.w #$0001,freetime		
		tst.b vb_flag
		bne vb_wait
		bra loop
		
delay22khz:
		nop
		dbf d0,delay22khz
		rts
		
addenemies:				;new enemy every five seconds
		tst.b added
		bne return 		;spam prevention
		move.b #$ff,added
		move.b seconds,d0
		andi.w #$0f,d0	;second digit only
		tst.b d0		;add a sprite on zero
		beq addsprite
		cmpi.b #$05,d0	;as well as five
		beq addsprite
		rts
randomnum:
		add.b #$01,random
		cmpi.b #$05,random
		bge resetrandom
		rts
resetrandom:
		move.b #$00,random
		bra randomnum
addsprite_debug:
		tst.b spawner
		beq return
addsprite:
		cmpi.w #$004F, enemies
		bge return
		moveq #$00000000,d0
		move.w enemies,d0
		move.w #$0501,d1
		lsl.w #$03,d0;multiply by 8
		lea (spritebuffer)+8,a5
		add.l d0,a5
		add.w #$01,enemies
		add.w enemies,d1
		
		move.b #$01, d4
		move.b enemies_bcd, d7
		abcd d4,d7 
        move.b d7, enemies_bcd  ;Decimal counter (for HUD)
		
		moveq #$00000000,d0
		move.b enemyframe,d0
		lsl.w #$02,d0	
		
		cmpi.b #$01,random
		beq add_privvio
		cmpi.b #$02,random
		beq add_zerodiv
		cmpi.b #$03,random
		beq add_addrerr
		cmpi.b #$04,random
		beq add_illegal
		
add_privvio:
		add.w #$6520,d0
		move.w #$0150,(a5)+
		move.w d1,(a5)+
		move.w d0,(a5)+
		move.w #$0080,(a5)+
		rts
add_zerodiv:
		add.w #$6540,d0
		move.w #$0080,(a5)+
		move.w d1,(a5)+
		move.w d0,(a5)+
		move.w #$01b0,(a5)+
		rts	
add_addrerr:
		add.w #$6530,d0
		move.w #$0080,(a5)+
		move.w d1,(a5)+
		move.w d0,(a5)+
		move.w #$0080,(a5)+
		rts	
add_illegal:
		add.w #$6550,d0
		move.w #$0150,(a5)+
		move.w d1,(a5)+
		move.w d0,(a5)+
		move.w #$01b0,(a5)+
		rts				
			
setup_sprites:
		;bra setup_sprites_stress
		lea (spritebuffer),a5
		move.w #$00e0,(a5)+
		move.w #$0a01,(a5)+
		move.w #$4500,(a5)+
		move.w #$0110,(a5)+
		rts

setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)       ;load setup array
        dbf d4,vram_Loop
        rts  
clear_regs:
		moveq #$00000000,d0
		move.l d0,d1
		move.l d0,d2
		move.l d0,d3
		move.l d0,d4
		move.l d0,d5
		move.l d0,d6
		move.l d0,d7
		move.l d0,a0
		move.l d0,a1
		move.l d0,a2
		move.l d0,a3
		move.l d0,a4
		move.l d0,a5
		move.l d0,a6
		rts
clear_ram:
		move.w #$7FF0,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
clear_vram:
		move.l #$40000000,(a3)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts		

HBlank:
		add.w #$01,hblanks
		move.l #$40000010,(a3) ;write to VSRAM
		move.w hblanks,d0    
		mulu.w d7,d0	   		   
		lsr #$06,d0	   
		sub.w d7, d0
		sub.w d7, d0
		move.w d0,(a4)   
		move.w d0,(a4)
		rte	

VBlank:
		tst.b splashflag
		bne calc_script		
		tst.b titleflag
		bne vblank_title
		movem.l d0/d1/d2/d3/d4/d5/d6/d7, -(sp)			
		bsr read_controller
		bsr compare_input
		bsr draw_hud
		bsr update_sprites	
		move.b #$00,vb_flag	
		movem.l (sp)+, d0/d1/d2/d3/d4/d5/d6/d7
        rte
		
calc_script:
	    move.w #$0000,hblanks 
	 	add.w #$0001,d7
		cmpi.w #$0000,d7
		beq end_splash
		rte
end_splash:
		move.w #$8AFF,(a3)	
        move.l #$40000010,(a3)   ;write to vsram   
		move.w #$0000,(a4)
		move.w #$0000,(a4)	
		move.b #$00,splashflag
		rte		

animate:
		add.b #$01,anim_timer
		cmpi.b #60, anim_timer
		blt return
		move.b #$00,anim_timer
		bsr animate_player
		bsr animate_enemies
		rts

animate_enemies:		
		lea (spritebuffer)+12,a5
		move.w enemies,d4
		cmpi.w #$0000,d4
		beq return		
		add.b #$01,enemyframe	
		cmpi.b #$04, enemyframe
		bgt resetenemyframe
enemyanimloop:
		add.w #$04,(a5)
		add.l #$08,a5
		dbf d4,enemyanimloop
		rts
resetenemyframe:
		move.b #$00, enemyframe
enemyresetloop:
		sub.w #$10,(a5)
		add.l #$08,a5
		dbf d4,enemyresetloop
		bra animate_enemies		

animate_player:		
		lea (spritebuffer)+4,a5
		add.w #$0009,playerframe
		move.w playerframe,(a5)
		cmpi.w #$451b,playerframe
		bgt resetplayerframe	
		rts
resetplayerframe:
		move.w #$4500,playerframe
		move.w playerframe,(a5)
		rts
		
draw_hud:		
		move.l #$40bc0003,(a3) ;C8bc
        move.b enemies_bcd,d0
		bsr writebyte	

		move.l #$40A60003,(a3) ;C8c6
        move.b seconds,d0
		bsr writebyte
		
		move.l #$40A00003,(a3) ;C8c0
        move.b minutes,d0
		bsr writebyte	
		
		tst.b debug
		beq return
		move.l #$48240003,(a3) ;C0a0
        move.w freetime,d0
		bsr writeword			
		rts
		
 include "track.asm"
		
collision:
		move.w enemies, d2
		cmpi.w #$0000,d2
		beq return		
		moveq #00000008,d0
		moveq #000000014,d1
collisionloop:		
		lea (spritebuffer),a0	;player y
		move.w (a0),d3
		lea (spritebuffer)+6,a0	;player x
		move.w (a0),d4			
		lea (spritebuffer),a0
		add.l d0,a0
		move.w (a0),d5
		lea (spritebuffer),a0
		add.l d1,a0		
		move.w (a0),d6	

		sub.w #$0018,d5
        cmp.w d3,d5
		 bge nocollide  
		add.w #$0018,d5	;return to original 

		sub.w #$0010,d3
        cmp.w d3,d5
		 ble nocollide  
		add.w #$0010,d5
			
		sub.w #$0018,d6
        cmp.w d4,d6
		 bge nocollide  
		add.w #$0018,d6	;return to original 

		sub.w #$0010,d4
        cmp.w d4,d6
		 ble nocollide  
		add.w #$0010,d6		
collide:
		tst.b invincible
		bne return
		tst.b boxflag
		bne return		
		lea (spritebuffer),a0
		add.l d0,a0
		add.l #$04,a0
		move.w (a0),d0		;let's figure out what killed us
		
		cmpi.w #$6554,d0
		bge end_illegal
		
		cmpi.w #$6530,d0
		ble end_privvio

		cmpi.w #$6534,d0
		beq end_adderr
		cmpi.w #$6538,d0
		beq end_adderr	
		cmpi.w #$653c,d0
		beq end_adderr	
		cmpi.w #$6540,d0
		beq end_adderr	

		cmpi.w #$6544,d0
		beq end_zerodiv	
		cmpi.w #$6548,d0
		beq end_zerodiv	
		cmpi.w #$654c,d0
		beq end_zerodiv	
		cmpi.w #$6550,d0
		beq end_zerodiv		
		
		bra no_tmss ;failsafe
check_box:
		tst.b safezone
		beq gethit
		lea (spritebuffer),a0
		cmpi.w #$00C8,(a0)
		ble gethit
		cmpi.w #$0100,(a0)
		bge gethit
		add.l #$06,a0
		cmpi.w #$00f8,(a0)
		ble gethit
		cmpi.w #$0130,(a0)
		bge gethit
		move.b #$ff,boxflag ;freeze timer, set invincible
		rts					;WE ARE IN THE BOX!
gethit:
		move.b #$00,boxflag ;unfreeze timer 
		rts
		
nocollide:		
		add.w #$08,d0
		add.w #$08,d1
		dbf d2, collisionloop
		rts
		
termtextloop:					;for strings with terminators
		move.b (a5)+,d4	
		cmpi.b #$24,d4			;$ (end of text flag)
		beq return
		andi.w #$00ff,d4
        move.w d4,(a4)		
		bra termtextloop			
		
update_sprites:
		lea (spritebuffer),a5
		move.l #$70000003,(a3)
		move.w #$00140,d4
spriteloop:
		move.w (a5)+,(a4)
		dbf d4, spriteloop
		rts
compare_input:
		move.b d3,d5
		or.b #$bf,d5
		cmpi.b #$bf,d5
		bne slow
		move.w #$07,speed	
		bra fast
slow:
		move.w #$04,speed	
fast:		
        move.b d3,d5    ;move d3 to d5 where it can be compared for input
		or.b #$fe,d5    ;$fe is controller reading if up is pressed
		cmpi.b #$fe,d5  ;check if up was pressed
		 beq up
chkdown:	    
		move.b d3,d5
		or.b #$fd,d5
		cmpi.b #$fd,d5
		 beq down
chkleft:		 
        move.b d3,d5
		or.b #$fb,d5
		 cmpi.b #$fb,d5
		 beq left
chkright:	    
		move.b d3,d5
		or.b #$f7,d5
		cmpi.b #$f7,d5
		 beq right	
chk_C:
		move.b d3,d5
		or.b #$df,d5
		cmpi.b #$df,d5
		 beq addsprite_debug			 
		rts
		
up:
	   lea (spritebuffer),a5
	   cmpi.w #$80,(a5)
	   ble return	 	   
	   move.w speed, d2
	   sub.w d2,(a5)
	   bra chkdown
down:
	   lea (spritebuffer),a5
	   cmpi.w #$148,(a5)
	   bge return	  	   
	   move.w speed, d2	   
	   add.w d2,(a5)
       bra chkleft		   
left:
	   lea (spritebuffer)+6,a5
	   cmpi.w #$80,(a5)
	   ble return	   
	   move.w speed, d2	   
	   sub.w d2,(a5)
	   bra chkright
right:
	   lea (spritebuffer)+6,a5
	   cmpi.w #$1a8,(a5)
	   bge return	   
	   move.w speed, d2	   
	   add.w d2,(a5)		
	   bra chk_C
	   
read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ; Set direction
	    move.b  #$40, ($A10003) ; TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	; d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		; d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ; TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		rts	
		
create_map:                      ;for 320x224 images
        move.l #$00002080,d0
        move.w #$002f,d5
        move.l #$40000003,(a3)   ;vram write to $C000	   
superloop:
        move.w #$27,d4
maploop:
        move.w d0,(a4)
        add.w #$1,d0
        dbf d4,maploop
        move.w #$17,d4
maploop2:
        move.w #$05c0,(a4)      ;blank tile
        dbf d4,maploop2
        dbf d5,superloop
        rts	

generate_map_splash:                  ;generate a map for splash
        move.w #$001c,d5
        move.l #$00008180,d0         ;first tile		
superloop_splash:
        move.w #$27,d4
maploop_splash:
        move.w d0,(a4)
        add.w #$1,d0
        dbf d4,maploop_splash
        move.w #$17,d4
maploop2_splash:
        move.w #$00,(a4)
        dbf d4,maploop2_splash
        dbf d5,superloop_splash
        rts			
		
unhold:
		bsr read_controller
		cmpi.b #$ff,d3
		 beq return
		bra unhold	

overlay_text:			;for strings with terminators
		move.b (a5)+,d4	
		cmpi.b #$24,d4	;$ (end of text flag)
		beq return
		andi.w #$00ff,d4
		eor.w #$4000,d4
        move.w d4,(a4)		
		bra overlay_text		
		
writeword:
	    move.w d0,d5
		andi.w #$f000,d5
		lsr #$8,d5
		lsr #$4,d5
		andi.w #$00ff, d5
    	add.w #$06f0,d5
		move.w d5,(a4)
	    move.w d0,d5
		andi.w #$0f00,d5
		lsr #$8,d5
		andi.w #$00ff, d5
    	add.w #$06f0,d5
		move.w d5,(a4)
writebyte:  ;not to be confused with litebrite				
	    move.w d0,d5
		andi.w #$00f0,d5
		lsr #$4,d5
    	add.w #$06f0,d5
		move.w d5,(a4)
	    move.w d0,d5
		andi.w #$000f,d5		
    	add.w #$06f0,d5	
		move.w d5,(a4)		
		rts	

calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts			
return:
		rts
returnint:
		rte
		
end_privvio:
		move.w #$0700,sr
		or.w #$2000,sr
		bra privilegevio  ;failsafe for emulators
end_adderr: ;ToDo: include force address error for emulators
		move.l #$FF00AB,a5
		move.w d5,(a5)+
		swap d5
		move.w d5,(a5)+
		bra addresserror  ;failsafe for emulators
end_zerodiv
		clr d7
		clr d3
		divu.w d3,d7
		bra zerodivide   ;failsafe for emulators
end_illegal:
		eor.w #$0030,d1
		lsr.w #$04,d1
		move.w d1,(a4)
		dc.w $4f75
		bra IllegalInstr ;failsafe for emulators
		

	include "titleloop.asm"	
	include "clock.asm"	
	include "music_driver.asm"	
	include "decompress.asm"	
	include "crashscreen.asm"	
	include "data.asm"

 dc.b "Pick up the receiver I'll make you a believer."

ROM_End:
              
              