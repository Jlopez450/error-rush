track:
		tst.b challenge
		beq hardmode
		eor.b #$ff,easyinverter
		tst.b easyinverter
		beq return
hardmode:		
		move.w enemies, d4
		cmpi.w #$0000,d4
		beq return
		sub.w #$01,d4		
		moveq #00000008,d5
		moveq #000000014,d6
trackloop:
		lea (spritebuffer),a6   ;ypos
		move.w (a6),d0
		lea (spritebuffer)+6,a6 ;xpos
		move.w (a6),d1		
		
		lea (spritebuffer),a6 ;ypos
		add.l d5,a6
		move.w (a6),d2
		lea (spritebuffer),a6;xpos
		add.l d6,a6		
		move.w (a6),d3		
		eor.b #$ff,inverter
		cmp.w d0,d2
		ble trackdown
		eor.b #$ff,inverter		
		cmp.w d0,d2
		bge trackup	
trackret:
		eor.b #$ff,inverter		
		cmp.w d1,d3
		bge trackleft
		eor.b #$ff,inverter		
		cmp.w d1,d3
		ble trackright
	
trackend:	
		add.w #$08,d5
		add.w #$08,d6	
		dbf d4,trackloop
		rts
		
trackdown:
		lea (spritebuffer),a6 ;ypos
		add.l d5,a6
		add.w #$1,(a6)
		bsr downslow
		 
		add.l #$04,a6
		cmpi.w #$6554,(a6) ;face enemy
		bgt trackfacedown
		cmpi.w #$6530,(a6) ;screen enemy	
		ble trackscreendown
		
		cmpi.w #$6544,(a6) ;spiral enemy
		beq trackspiraldown
		cmpi.w #$6548,(a6) ;spiral enemy
		beq trackspiraldown	
		cmpi.w #$654c,(a6) ;spiral enemy
		beq trackspiraldown	
		cmpi.w #$6550,(a6) ;spiral enemy
		beq trackspiraldown		
		bra trackret 
downslow:
		tst.b inverter
		beq downslow2	
		add.w #$01,(a6)			
		rts
downslow2:
		sub.w #$01,(a6)			
		rts		
		
trackfacedown:		
		sub.l #$04,a6		
		add.w #$1,(a6)				
		bra trackret 
trackscreendown:
		sub.l #$04,a6		
		add.w #$1,(a6)				
		bra trackret 
trackspiraldown:
		sub.l #$04,a6		
		move.b (a6),d7
		lsl.w #$01,d7
		add.w d7,(a6)	
		bra trackret 
		
trackup:
		lea (spritebuffer),a6 ;ypos
		add.l d5,a6		
		sub.w #$1,(a6)
		bsr upslow	
	 
		add.l #$04,a6
		cmpi.w #$6554,(a6) ;face enemy
		bgt trackfaceup
		cmpi.w #$6530,(a6) ;screen enemy	
		ble trackscreenup	
		
		cmpi.w #$6544,(a6) ;spiral enemy
		beq trackspiralup
		cmpi.w #$6548,(a6) ;spiral enemy
		beq trackspiralup	
		cmpi.w #$654c,(a6) ;spiral enemy
		beq trackspiralup	
		cmpi.w #$6550,(a6) ;spiral enemy
		beq trackspiralup			
		bra trackret 
upslow:
		tst.b inverter
		beq upslow2	
		sub.w #$01,(a6)			
		rts
upslow2:
		add.w #$01,(a6)			
		rts	
		
trackfaceup:
		sub.l #$04,a6		
		sub.w #$1,(a6)		 		
		bra trackret  
trackscreenup:
		sub.l #$04,a6		
		sub.w #$1,(a6)				
		bra trackret 
trackspiralup:
		sub.l #$04,a6		
		move.b (a6),d7
		lsl.w #$01,d7		
		sub.w d7,(a6)	
		bra trackret 
		
		
trackleft:		
		lea (spritebuffer),a6 ;xpos
		add.l d6,a6		
		sub.w #$1,(a6)
		bsr leftslow	
		 
		sub.l #$02,a6
		cmpi.w #$6554,(a6) ;face enemy
		bgt trackfaceleft

		cmpi.w #$6544,(a6) ;spiral enemy
		beq trackspiralleft
		cmpi.w #$6548,(a6) ;spiral enemy
		beq trackspiralleft	
		cmpi.w #$654c,(a6) ;spiral enemy
		beq trackspiralleft	
		cmpi.w #$6550,(a6) ;spiral enemy
		beq trackspiralleft			
		bra trackend  	
leftslow:
		tst.b inverter
		beq leftslow2		
		sub.w #$01,(a6)			
		rts
leftslow2:		
		add.w #$01,(a6)			
		rts
		
trackfaceleft:
		add.l #$02,a6		
		sub.w #$1,(a6)		 		
		bra trackend
trackspiralleft:
		add.l #$02,a6		
		move.b (a6),d7
		lsl.w #$01,d7	
		sub.w d7,(a6)	
		bra trackend 
		
trackright:		
		lea (spritebuffer),a6 ;xpos
		add.l d6,a6		
		add.w #$1,(a6)
		bsr rightslow	
		 
		sub.l #$02,a6
		cmpi.w #$6554,(a6) ;face enemy
		bgt trackfaceright	
		
		cmpi.w #$6544,(a6) ;spiral enemy
		beq trackspiralright
		cmpi.w #$6548,(a6) ;spiral enemy
		beq trackspiralright	
		cmpi.w #$654c,(a6) ;spiral enemy
		beq trackspiralright	
		cmpi.w #$6550,(a6) ;spiral enemy
		beq trackspiralright		
		bra trackend  	
rightslow:
		beq rightslow2		
		add.w #$01,(a6)			
		rts
rightslow2:		
		sub.w #$01,(a6)			
		rts
		
trackfaceright:	
		add.l #$02,a6		
		add.w #$1,(a6)		 		
		bra trackend  
trackspiralright:
		add.l #$02,a6		
		move.b (a6),d7
		lsl.w #$01,d7	
		add.w d7,(a6)	
		bra trackend 