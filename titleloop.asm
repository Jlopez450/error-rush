titlescreen:
		move.b #$ff,titleflag
		; lea (music1+40),a2
		; move.l a2,vgm_start		
		move.b #$01,page_ID
		bsr new_page
		move.w #$C9BE,cursorpos				
		move.w #$2300,sr
titleloop:
		cmpi.b #$7f,d3
		 beq end_title	 
        bsr read_controller	
		bra titleloop

vblank_title:
		bsr titlecontrol
		;bsr music_driver					
		rte

titlecontrol:	
		cmpi.b #$F7, d3
		beq pageright		
		cmpi.b #$01, page_ID
		 beq selection	
		cmpi.b #$FB, d3
		beq pageleft		 
		rts
toggleeasy:
		eor.b #$ff,challenge
		move.l #$49b60003,(a3)		
		tst.b challenge
		bne seton
		bra setoff		
toggleinvinc:
		eor.b #$ff,invincible
		move.l #$4a360003,(a3)		
		tst.b invincible
		bne seton
		bra setoff
toggleCPU:	
		eor.b #$ff,debug
		move.l #$4ab60003,(a3)			
		tst.b debug
		bne seton
		bra setoff
togglespawner:
		move.l #$4b360003,(a3)	
		eor.b #$ff,spawner
		tst.b spawner
		bne seton
		bra setoff
togglesafe:
		move.l #$4bb60003,(a3)	
		eor.b #$ff,safezone
		tst.b safezone
		bne seton
		bra setoff
seton:		
		move.w #$004f,(a4)
		move.w #$004e,(a4)
		move.w #$0020,(a4)
		bra unhold
setoff:		
		move.w #$004f,(a4)
		move.w #$0046,(a4)
		move.w #$0046,(a4)
		bra unhold	
		
pageleft:
		cmpi.b #$01,page_ID
		beq return
		sub.b #$01,page_ID
		bsr new_page
		bsr unhold
		rts
pageright:
		cmpi.b #$03,page_ID
		beq return
		add.b #$01,page_ID
		bsr new_page
		bsr unhold		
		rts
			
new_page:
		lea pages,a0		
		moveq #$00000000,d0
		move.b page_ID,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)

pages:
	dc.w page1
	dc.w page2
	dc.w page3

page1:
		move.l #$0000c000,d0
		bsr calc_vram
		move.l d0,(a3)
		lea (titletext),a5
		bsr smalltextloop
		rts
page2:	
		move.l #$0000c000,d0
		bsr calc_vram
		move.l d0,(a3)
		lea (enemytext),a5
		bsr smalltextloop
		move.l #$0000c12e,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$e534,(a4)
		move.w #$e536,(a4)		
		move.l #$0000c1ae,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$e535,(a4)
		move.w #$e537,(a4)

		move.l #$0000c22e,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$e544,(a4)
		move.w #$e546,(a4)
		move.l #$0000c2ae,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$e545,(a4)
		move.w #$e547,(a4)	

		move.l #$0000c32e,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$e524,(a4)
		move.w #$e526,(a4)	
		move.l #$0000c3ae,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$e525,(a4)
		move.w #$e527,(a4)

		move.l #$0000c42e,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$e55c,(a4)
		move.w #$e55e,(a4)	
		move.l #$0000c4ae,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$e55d,(a4)
		move.w #$e55f,(a4)			
		rts		
page3:	
		move.l #$0000c000,d0
		bsr calc_vram
		move.l d0,(a3)
		lea (abouttext),a5
		bsr smalltextloop
		rts				
	
smalltextloop:
		move.w #$0027,d4		;draw 40 cells of text
		clr d5	
textloop:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return	
		cmpi.b #$20,d5
		 beq clearspace			;band-aid fix
clear_return:		 
		andi.w #$00ff,d5
		;or.w #$4000,d5
        move.w d5,(a4)	
		dbf d4,textloop	
		move.w #$0017,d4	    ;draw 24 cells of nothing
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		bra smalltextloop
clearspace:
		move.b #$00,d5
		bra clear_return	
	
end_title:
		move.w #$2700,sr
		move.b #$00,titleflag	
		rts

selection:
		tst.b d3
		bne return
		bsr read_controller
		bsr selection_control
		moveq #$00000000,d5
		moveq #$00000000,d0
		move.w cursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$003c,(a4)
		move.w #$002d,(a4)
		move.w #$002d,(a4)	
		moveq #$00000000,d0		
		move.w oldcursorpos,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0000,(a4)	
		move.w #$0000,(a4)	
		move.w #$0000,(a4)
		rts
selection_control:	
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf, d7
		beq togglething	
		move.b d3,d7
		or.b #$Fe,d7
		cmpi.b #$Fe, d7
		beq cursorup	
		move.b d3,d7
		or.b #$Fd,d7		
		cmpi.b #$Fd, d7
		beq cursordown	
		rts	
togglething:
		cmpi.w #$C9be,cursorpos	
		beq toggleeasy
		cmpi.w #$Ca3e,cursorpos	
		beq toggleinvinc
		cmpi.w #$CaBE,cursorpos	
		beq togglecpu
		cmpi.w #$CB3E,cursorpos	
		beq togglespawner
		cmpi.w #$CBBE,cursorpos	
		beq togglesafe
		rts

cursorup:
		cmpi.w #$C9be,cursorpos	
		beq return
		move.w cursorpos,oldcursorpos	
		sub.w #$0080,cursorpos
		bsr unhold
		rts
cursordown:
		cmpi.w #$CBBE,cursorpos	
		beq return
		move.w cursorpos,oldcursorpos
		add.w #$0080,cursorpos		
		bsr unhold		
		rts		
		